# cURLsrv
Web server that hosts cURL-able terminal animations. This is nothing more than
a small Flask app that hosts files at a fixed transfer rate. There's also some
user agent filtering to keep non-cURL clients out.

![Terminal showing cURL fetching the included index file](screenshot.gif)

Files are served "throttled" with a delay between each byte to simulate transfer
over a slow link. Speed is set in [baud](https://en.wikipedia.org/wiki/Baud)
with the `baud` URL query parameter (default is 9600, but each text file can be
configured independently).


## Requirements
You will need Python 3 and a web server (Nginx example included). The commands
in this file were ran as root on Debian 11.

```shell
apt install git gcc python3 python3-venv nginx
```

The included `uwsgi.conf` runs the application under the `curlsrv` user.

```shell
useradd -r curlsrv
```


## Installation
Install directories for the application to use:

```shell
install -m 755 -d /var/www/curlsrv
install -m 755 -o curlsrv -g curlsrv -d /var/log/curlsrv
```

Clone this repo into the install location:

```shell
cd /var/www/curlsrv
git clone --depth 1 https://gitlab.com/unixispower/curlsrv.git .
```

Create a virtual environment (venv) and install this package to it:

```shell
python3 -m venv venv
. venv/bin/activate
pip install --upgrade pip setuptools uwsgi
pip install -e .
deactivate
```

Copy and enable the service file if you wish to manage the application with
systemd:

```shell
install -m 644 curlsrv.service /etc/systemd/system
systemctl daemon-reload
systemctl enable curlsrv.service
systemctl enable nginx.service
```

You will also need to configure Nginx to proxy uWSGI. See the file
`nginx.example` for a server configuration that can be placed in
`/etc/nginx/sites-avaliable` (and later symlinked under
`/etc/nginx/sites-enabled` to be enabled).


## Configuration
The directory `text` holds files that are served. These files can be plain text
files or fancy things like VT100 animations. Files are handled as a stream of
bytes so any character encoding can be used.

Transfer speed and query parameter ranges can be set for all files in
`text/global.conf`. Each file in `text` can also have a corresponding `.conf`
file that overrides the global configuration (useful for setting different
default transfer rates for different files).

The file `uwsgi.conf` holds the server configuration. See the
[uwsgi docs](https://uwsgi-docs.readthedocs.io/en/latest/) for more information.


## Running
Start the services for Nginx and cURLsrv:

```shell
systemctl start curlsrv.service
systemctl start nginx.service
```


## Licensing
cURLsrv is licensed under the 2-clause BSD license, see LICENSE for details.

The file `text/frogs.vt` was found on
[artscene.textfiles.com](http://artscene.textfiles.com/vt100/).
