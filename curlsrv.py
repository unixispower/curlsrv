#!/usr/bin/env python3
# Web server that hosts cURL-able terminal animations

import flask
import werkzeug.exceptions
import werkzeug.utils

import configparser
import logging.config
import os.path
import time


TEXT_ROOT = 'text'  # path to directory containing text files
INDEX_FILENAME = 'index' # name of file to load for /

CONFIG_FILENAME = 'global.conf'  # name of the global configuration
CONFIG_EXT = '.conf'  # extension of configuration files

BAUD_DEFAULT = 9600
BAUD_MIN = 75
BAUD_MAX = 115200


# configure logging to wsgi stream
logging.config.dictConfig({
    'version': 1,
    'formatters': {
        'default': {
            'format': '%(asctime)s - %(levelname)s - %(message)s',
        }
    },
    'handlers': {
        'wsgi': {
            'class': 'logging.StreamHandler',
            'stream': 'ext://flask.logging.wsgi_errors_stream',
            'formatter': 'default'
        }
    },
    'root': {
        'level': 'INFO',
        'handlers': ['wsgi']
    }
})

app = flask.Flask(__name__)


@app.route('/', methods=['GET'])
@app.route('/<path:path>', methods=['GET'])
def handle_request(path=''):
    """Serve the text file that corresponds to the request path"""
    # reject request if not made with curl
    user_agent = flask.request.headers.get('User-Agent', '')
    if 'curl' not in user_agent.casefold():
        message = f"Inferior user agent; use curl."
        raise werkzeug.exceptions.BadRequest(message)

    # reject unsafe paths
    text_path = werkzeug.utils.safe_join(TEXT_ROOT, path)
    if text_path is None:
        raise werkzeug.exceptions.BadRequest("Bad path")

    # don't serve configurations
    if text_path.endswith(CONFIG_EXT):
        raise werkzeug.exceptions.NotFound()

    # add index filename to directory paths
    if os.path.isdir(text_path):
        text_path = werkzeug.utils.safe_join(text_path, INDEX_FILENAME)

    # load and serve the text file
    try:
        with open(text_path, mode='rb') as text_file:
            text_bytes = text_file.read()
    except FileNotFoundError:
        raise werkzeug.exceptions.NotFound()

    # load configuration
    global_config = load_config(os.path.join(TEXT_ROOT, CONFIG_FILENAME))
    file_config = load_config(text_path + CONFIG_EXT, global_config)
    baud_rate = file_config.getint('text', 'baud', fallback=BAUD_DEFAULT)
    baud_min = file_config.getint('text', 'baud_min', fallback=BAUD_MIN)
    baud_max = file_config.getint('text', 'baud_max', fallback=BAUD_MAX)

    # get baud rate from query parameter
    baud_rate = flask.request.args.get('baud', baud_rate, type=int)
    if baud_rate < baud_min or baud_rate > baud_max:
        raise werkzeug.exceptions.BadRequest(
            f"Baud rate must be between {baud_min} and {baud_max}")

    # serve file contents at a constant speed
    generator = throttle_text(text_bytes, baud_rate)
    return flask.Response(generator, mimetype='application/octet-stream')


@app.errorhandler(werkzeug.exceptions.HTTPException)
def handle_error(error):
    """Render errors as plain text"""
    message = f"Error {error.code}: {error.description}\n"
    return flask.Response(message, error.code, mimetype='text/plain')


def throttle_text(text_bytes, baud_rate):
    """Yield each byte in text_bytes with a delay between each"""
    bytes_per_sec = baud_rate / 10  # baud_rate / (1 start + 8 data + 1 stop)
    delay_secs = 1 / bytes_per_sec
    for i in range(len(text_bytes)):
        time.sleep(delay_secs)
        yield text_bytes[i:i+1]

def load_config(path, fallback_config=None):
    """Load a config file, use fallback_config for missing values"""
    config = configparser.ConfigParser()
    if fallback_config is not None:
        config.read_dict(fallback_config)
    config.read(path)
    return config
