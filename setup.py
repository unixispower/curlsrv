import setuptools


setuptools.setup(
    name='curlsrv',
    version='0.1.0',
    description="Web server that hosts cURL-able terminal animations",
    url='https://gitlab.com/unixispower/curlsrv',
    author='Blaine Murphy',
    author_email='myself@blaines.world',
    py_modules=['curlsrv'],
    zip_safe=False,
    install_requires=['flask'],
)
